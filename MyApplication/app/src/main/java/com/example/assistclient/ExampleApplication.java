package com.example.assistclient;

import android.app.Application;
import com.alicecallsbob.assist.sdk.core.AssistApplication;
import com.alicecallsbob.assist.sdk.core.AssistCore;
import com.alicecallsbob.assist.sdk.core.AssistCoreImpl;

public class ExampleApplication extends Application implements AssistApplication {

    // Our Activity must manage the lifecycle of the AssistCore instance - so
    // we must maintain a reference to it!
    private AssistCore assistCore;

    @Override
    public void onCreate() {
        super.onCreate();

        // construct build the AssistCore instance
        this.assistCore = new AssistCoreImpl(this);
    }


    @Override
    public void onTerminate() {
        super.onTerminate();

        // terminate the AssistCore
        this.assistCore.terminate();
        this.assistCore = null;
    }

    ///////////////////////////////////
    //
    // AssistApplication method

    // returns the instance of the AssistCore that is managed by this class
    @Override
    public AssistCore getAssistCore() {
        return this.assistCore;
    }
}
