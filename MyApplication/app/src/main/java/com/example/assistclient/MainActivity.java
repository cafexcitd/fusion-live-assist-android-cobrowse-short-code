package com.example.assistclient;

import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.alicecallsbob.assist.sdk.config.AssistConfig;
import com.alicecallsbob.assist.sdk.config.impl.AssistCobrowseAuthEvent;
import com.alicecallsbob.assist.sdk.config.impl.AssistCobrowseAuthListener;
import com.alicecallsbob.assist.sdk.config.impl.AssistConfigBuilder;
import com.alicecallsbob.assist.sdk.core.Assist;
import com.alicecallsbob.assist.sdk.core.AssistError;
import com.alicecallsbob.assist.sdk.core.AssistListener;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class MainActivity extends AppCompatActivity implements AssistListener {

    // we will need a reference to the 'Help!' button, since we will enable
    // and disable it according to whether we are on a support session or not
    private Button helpButton = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.activity_main);

        // this class will act as AssistListener
        final AssistListener assistListener = this;

        // wire up the 'Help!' button click handler
        this.helpButton = (Button) this.findViewById(R.id.help);
        this.helpButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // while the Assist object will gracefully handle additional
                // invocations of the startSupport method, we will cause this button
                // to become un-touchable while the support session is active
                helpButton.setEnabled(false);

                // setup a request queue
                final RequestQueue queue = Volley.newRequestQueue(getApplication());

                // start a background task as Android restricts long running tasks
                // from being executed on the UI thread
                String url = "http://rp.example.com:8080/assistserver/shortcode/create";
                queue.add(
                    new JsonObjectRequest(Request.Method.PUT, url, null, new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject shortCodeResponse) {

                            try {
                                // get & display the code!
                                final String shortCode = shortCodeResponse.getString("shortCode");
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        // display the code with a Toast
                                        Toast.makeText(
                                                getApplication(),
                                                "Code: " + shortCode,
                                                Toast.LENGTH_LONG).show();
                                    }
                                });

                                // now we have the short code - get the session token!
                                String url = "http://rp.example.com:8080/assistserver/shortcode/consumer";
                                url += "?appkey=" + shortCode;


                                queue.add(
                                    new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
                                        @Override
                                        public void onResponse(JSONObject response) {
                                            try {
                                                // now we have the session token, extract the params
                                                // that we need...
                                                final String correlationId = response.getString("cid");
                                                final String sessionToken = response.getString("session-token");

                                                // ... and use them to configure the assist config
                                                AssistConfig config = new AssistConfigBuilder(getApplicationContext())
                                                        .setCorrelationId(correlationId)
                                                        .setSessionToken(sessionToken)
                                                        .setAgentName("agent1")
                                                        .setCobrowseAuthListener(new AssistCobrowseAuthListener() {
                                                            // we will configure the app to automatically accept the
                                                            // assist session offered
                                                            @Override
                                                            public void onCobrowseRequested(AssistCobrowseAuthEvent assistCobrowseAuthEvent) {
                                                                assistCobrowseAuthEvent.acceptCobrowse();
                                                            }
                                                        })
                                                        .build();

                                                // now start the support session!
                                                Assist.startSupport(config, getApplication(), assistListener);
                                            }
                                            catch (Exception e) {
                                                e.printStackTrace();
                                            }
                                        }
                                    }, null)
                                );

                            }
                            catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }, null)
                );
            }
        });

        // wire up the end Support button
        Button endButton = (Button) this.findViewById(R.id.end);
        endButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Assist.endSupport();
            }
        });
    }


    ///////////////////////////////
    //
    // AssistListener methods

    @Override
    public void onSupportEnded(boolean b) {
        this.helpButton.setEnabled(true);
    }

    @Override
    public void onSupportError(AssistError assistError, String s) {
        this.helpButton.setEnabled(true);
    }
}
